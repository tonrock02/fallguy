﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private RespawnManager rm;
    
    void Start()
    {
        rm = GameObject.FindGameObjectWithTag("rm").GetComponent<RespawnManager>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            rm.lastCheckPoint = transform.position;

            Destroy(this.gameObject);
        }
    }

}
