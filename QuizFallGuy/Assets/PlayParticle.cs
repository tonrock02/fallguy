﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayParticle : MonoBehaviour
{
    [SerializeField] ParticleSystem Particle1;
    [SerializeField] ParticleSystem Particle2;
    [SerializeField] ParticleSystem Particle3;
    [SerializeField] ParticleSystem Particle4;
    [SerializeField] ParticleSystem Particle5;
    [SerializeField] ParticleSystem Particle6;
    [SerializeField] ParticleSystem Particle7;


    public void Playparticle()
    {
        Particle1.Play();
        Particle2.Play();
        Particle3.Play();
        Particle4.Play();
        Particle5.Play();
        Particle6.Play();
        Particle7.Play();
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Playparticle();
        }
    }
}
