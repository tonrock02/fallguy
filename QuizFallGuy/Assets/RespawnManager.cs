﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnManager : MonoBehaviour
{
    private static RespawnManager instance;
    public Vector3 lastCheckPoint;
    public GameObject Player;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Update()
    {
        if (Player.gameObject.transform.position.y < -7)
        {
            Respawn();
        }
    }
    void Respawn()
    {
        Player.gameObject.transform.position = lastCheckPoint;
    }
}
